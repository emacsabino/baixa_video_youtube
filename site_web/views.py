from django.shortcuts import render
from django.http import HttpResponse
from pytube import YouTube
from django.views.generic.base import View
import time


# Create your views here.
site = None

class Abertura(View):

    endereco=None
    def get(self, request):
        if request.is_ajax():
            id_video = request.GET.get('message')
            self.baixar_video(id_video,request)


        return render(request, 'site_web/1.html',{})
    def post(self,request):
        global site
        self.endereco = request.POST['endereco']
        site = self.endereco
        img,list = listar_disponibilidades(self.endereco)


        return render(request, 'site_web/1.html', {'endereco': list,
                                                   'capa_img':img})

    def baixar_video(self,pk,request):
        millis = round(time.time() * 1000)
        VIDEO_URL = site + '='
        yt = YouTube(VIDEO_URL)
        #video = yt.streams[pk]

        video = yt.streams[int(pk)]
        if video.resolution!=None:
            caminho='videos/'+str(millis)+"_resolution_"+video.resolution
        else:
            caminho = 'videos/' + str(millis) + "_resolution_" + "audio"
        path_video = caminho +'/'+str(video.default_filename)
        video.download(caminho)



def listar_disponibilidades(endereco):
    lista = []
    VIDEO_URL = endereco + '='

    yt = YouTube(VIDEO_URL)
    imagem = yt.thumbnail_url
    indice =0

    for stream in yt.streams:

        tex = str(stream).split('"')
        extensao = tex[3].split('/')[1]
        if tex[9]=="False":
            tex[9]="audio"
        dados=[("indice",indice),("type",extensao),("resolucao",tex[5]),("codec",tex[9])]
        lista.append(dict(dados))
        indice=indice+1



    return imagem,lista


